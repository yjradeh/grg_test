<?php

namespace grg;

/*
 * Base class for all game's types
 * @author Youssef Jradeh <youssef.jradeh@gmail.com>
 */
class Player
{

    /*
    * Hold the name of the player
    */
    private $name = '';
    /*
    * Hold the current cards of the player
    */
    private $currentCards = [];

    /**
     * used to set the name of the player
     *
     * @param string $name
     *
     * @throws \Exception
     */
    public function __construct($name = '')
    {
        //fire an exception for wrong name
        if (! empty($name) && strlen($name) < 3) {
            throw new \Exception("Name is required");
        }
        $this->setName($name);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * used to add new card to the current cards
     *
     * @param $card
     *
     * @return void
     */
    public function addCard($card)
    {
        array_push($this->currentCards, $card);
    }

    /**
     * used to add an array of cards to the current cards
     *
     * @param $cards
     *
     * @return void
     */
    public function addCards($cards)
    {
        $this->currentCards = array_merge($this->currentCards, $cards);
    }

    /**
     * used to check if player has no card or to get the current card
     *
     * @return mixed
     */
    public function getCurrentCard()
    {

        //check if current player is a loser
        if (! $this->getCurrentCardsCount()) {
            return false;
        }

        return current($this->currentCards);
    }

    /**
     * remove the current card from the player current card
     *
     * @return bool
     */
    public function removeCurrentCard()
    {
        array_shift($this->currentCards);

        return true;
    }

    /**
     * used to check if player has no card or to get the count of his/her current cards
     *
     * @return mixed
     */
    public function getCurrentCardsCount()
    {
        return count($this->currentCards);
    }
}
