<?php

namespace grg;

/*
 * The Snap Game class
 *
 * @author Youssef Jradeh <youssef.jradeh@gmail.com>
 */
use grg\Base\BaseGame;

class Snap extends BaseGame
{

    /*
    * Hold the current stack cards
    */
    public $stackCards = [];

    /**
     * used to initiate game class and distribute the card to all players
     *
     * @param array $players
     *
     * @throws \Exception
     */
    public function __construct(array $players = [])
    {

        if (empty($players) || count($players) < 2) {
            throw new \Exception("At least two players are required to kick off the Snap game");
        }
        parent::__construct(new FrenchDeck(), $players);

        //distributed the cards
        $this->distributedCards();
    }

    /**
     * Distribute cards based on the number of player and number of cards for the specific deck type
     *
     * @return void
     */
    protected function distributedCards()
    {

        // get the number opf card for each player
        $cardPerPlayer = floor(count($this->deck->unDistributedCards) / count($this->players));

        //not sure if this required, but to be fair we should distribute the same number of card for each player
        $totalCardPerPlayers = count($this->players) * $cardPerPlayer;

        //remove additional card
        $this->deck->unDistributedCards = array_slice($this->deck->unDistributedCards, 0, $totalCardPerPlayers);

        // distribute cards to users
        foreach ($this->players as $player) {
            /* @var $player Player */
            for ($i = 0; $i < $cardPerPlayer; $i++) {
                $player->addCard($this->deck->distributedCard());
            }
        }
    }

    /**
     * Add card to stack
     *
     * @param $card
     *
     * @return void
     */
    public function addToStack($card)
    {
        array_push($this->stackCards, $card);
    }

    /**
     * Check current card with the latest card in the stack by comparing the value of both cards
     *
     * @param $card
     *
     * @return bool
     */
    public function match($card)
    {
        if (count($this->stackCards) < 1) {
            return false;
        }
        $lastCard = end($this->stackCards);
        if ($card['value'] == $lastCard['value']) {
            return true;
        }

        return false;
    }

    /**
     * Main function to run the game
     *
     * @return bool
     */
    public function start()
    {

        $numberOfSnap = 0;
        while (count($this->players) > 0) {
            foreach ($this->players as $key => $player) {
                /* @var $player Player */
                //check if the count of cart for player is equal to the total number of cards
                if (count($this->players) == 1) {
                    echo "Player " . $player->getName() . " is the winner after $numberOfSnap snap<br>";
                    $this->players = 0;
                    continue;
                }

                //put card on stack
                if (! $currentCard = $player->getCurrentCard()) {
                    echo "Player " . $player->getName() . " is a loser <br>";
                    unset($this->players[$key]);
                    continue;
                }

                // check if match
                if ($this->match($currentCard)) {
                    $player->addCards($this->stackCards);
                    $this->stackCards = [];
                    $numberOfSnap++;
                } else {
                    $player->removeCurrentCard();
                    array_push($this->stackCards, $currentCard);
                }
            }
        }

        return true;
    }
}
