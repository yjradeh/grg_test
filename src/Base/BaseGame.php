<?php

namespace grg\Base;

/*
 * Base class for all game's types
 * @author Youssef Jradeh <youssef.jradeh@gmail.com>
 */

use grg\Player;

abstract class BaseGame
{

    /*
    * hold all the players' objects
    *
    * @var Player[]
    */
    protected $players = [];

    /*
    * hold the deck object
    */
    protected $deck;

    /**
     * used to add all players to the game and specify the deck type to be used
     *
     * @param array $players
     * @param BaseDeck $deck
     *
     * @throws \Exception
     */
    public function __construct(BaseDeck $deck, $players = [])
    {

        //Check if player parameter sent
        if (! empty($players) && ! is_array($players)) {
            throw new \Exception("Players parameter is required");
        }
        //then add all the player
        $this->addPlayers($players);

        //assign the deck  object
        $this->deck = $deck;
    }

    /**
     * @return array
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * @param array $players
     */
    public function setPlayers($players)
    {
        $this->players = $players;
    }

    /**
     * @return BaseDeck
     */
    public function getDeck()
    {
        return $this->deck;
    }

    /**
     * @param BaseDeck $deck
     */
    public function setDeck($deck)
    {
        $this->deck = $deck;
    }

    /**
     * Add array of players
     *
     * @param $players
     *
     * @return void
     * @throws \Exception
     */
    public function addPlayers($players)
    {
        if (! count($players)) {
            return;
        }
        foreach ($players as $player) {
            // make sure of the player type
            if (! $player instanceof Player) {
                throw new \Exception("incorrect player type");
            }

            $this->addPlayer($player);
        }
    }

    /**
     * Add new player
     *
     * @param Player $player
     *
     * @return void
     */
    private function addPlayer(Player $player)
    {
        array_push($this->players, $player);
    }


    /**
     * Abstract function to oblige child classes to add the start function
     */
    abstract public function start();
}
