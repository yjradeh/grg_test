<?php

namespace grg\Base;

/*
 * Base class for all deck's types
 * @author Youssef Jradeh <youssef.jradeh@gmail.com>
 */

abstract class BaseDeck
{

    /*
    * Type of the card like Clubs or Diamonds
    */
    protected $types = [];

    /*
    * value of the card
    */
    protected $values = [];

    /*
    * hold all distributed Cards
    */
    public $distributedCards = [];

    /*
    * hold all unDistributed Cards
    */
    public $unDistributedCards = [];

    /**
     * used to generate all the cards based of the type and value properties
     * used the native shuffle function to shuffle the cards
     *
     * @throws \Exception
     */
    public function __construct()
    {
        //validation
        if (empty($this->types) || empty($this->values)) {
            throw new \Exception("Types and Values are required to create the deck");
        }

        $counter = 0;
        //assign $undistributedCards
        foreach ($this->types as $type) {
            foreach ($this->values as $value) {
                array_push($this->unDistributedCards, ['id' => ++$counter, 'type' => $type, 'value' => $value]);
            }
        }

        //shuffle the cards
        shuffle($this->unDistributedCards);
    }

    /**
     * remove card from the unDistributedCards and add it to the distributedCards
     *
     * @return mixed
     */
    public function distributedCard()
    {

        //check if unDistributedCards is empty
        if (count($this->unDistributedCards) < 1) {
            return false;
        }

        //move from unDistributedCards to distributedCards
        $card = array_shift($this->unDistributedCards);
        array_push($this->distributedCards, $card);

        return $card;
    }

    /**
     * return the total count for all deck [ some of the cards are removed if the number of player is odd
     *
     * @return int
     */
    public function getDeckCardCount()
    {

        return count($this->unDistributedCards) + count($this->distributedCards);
    }
}
