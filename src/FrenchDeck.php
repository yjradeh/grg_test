<?php

namespace grg;

/*
 * Base class for all game's types
 * @author Youssef Jradeh <youssef.jradeh@gmail.com>
 */
use grg\Base\BaseDeck;

class FrenchDeck extends BaseDeck
{
    protected $types = ['Clubs', 'Diamonds', 'Hearts', 'Spades'];
    protected $values = ['Ace', "2", "3", "4", "5", "6", "7", "8", "9", "10", 'Jack', 'Queen', 'King'];
}
