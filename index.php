<?php

require_once __DIR__ . '/vendor/autoload.php';

use grg\Player;
use grg\Snap;

$player1 = new Player("Youssef");
$player2 = new Player("Test");
$player3 = new Player("Test1");

try {
    $snap = new Snap([$player1, $player2, $player3]);
    $snap->start();
} catch (Exception $exception) {
    echo $exception->getMessage();
}
